﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelButton : MonoBehaviour
{

    [SerializeField]
    private List<Image> UIEnergyImages;

    public Image UIEnergyPrefab;

    public Transform UIEnergyPosition;

    private void Start()
    {
        GameManager.Instance.BubbleHit += EnergyLost;
        GameManager.Instance.GameStart += EnergyRestored;

        if (EnergyCount() != UIEnergyImages.Count)
        {
            EnergyRestored();
        }
    }

    private void OnDestroy()
    {
        GameManager.Instance.BubbleHit -= EnergyLost;
        GameManager.Instance.GameStart -= EnergyRestored;
    }


    private int EnergyCount()
    {
        return GameManager.SessionData.Energy;
    }

    private void EnergyLost()
    {
        int index = UIEnergyImages.Count - 1;
        Image img = UIEnergyImages[index];
        UIEnergyImages.RemoveAt(index);
        Destroy(img.gameObject);
    }

    private void EnergyGained()
    {
        Image img = Instantiate(UIEnergyPrefab, UIEnergyPosition);
        UIEnergyImages.Add(img);
    }

    private void EnergyRestored()
    {
        for (int i = 0; i < EnergyCount(); i++)
        {
            EnergyGained();
        }

    }
}
