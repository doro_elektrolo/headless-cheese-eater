﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CharacterController : MonoBehaviour
{

    [SerializeField] private float JumpForce = 400f;                         
    [Range(0, 1)] [SerializeField] private float CrouchSpeed = .36f;          
    [Range(0, .3f)] [SerializeField] private float MovementSmoothing = .05f;  
    [SerializeField] private bool AirControl = false;                         
    [SerializeField] private LayerMask WhatIsGround;                        
    [SerializeField] private Transform GroundCheck;                          
    [SerializeField] private Transform CeilingCheck;                          
    [SerializeField] private Collider2D CrouchDisableCollider;                

    const float GroundedRadius = .2f; 
    private bool Grounded;            
    const float CeilingRadius = .2f; 
    private bool FacingRight = true; 
    private Vector3 velocity = Vector3.zero;
    private Animator playerAnimator;
    private Rigidbody2D rigidbody;


    private void Awake()
    {
        playerAnimator = GetComponent<Animator>();
        rigidbody = GetComponent<Rigidbody2D>();
    }


    private void FixedUpdate()
    {
        Grounded = false;

      
        Collider2D[] colliders = Physics2D.OverlapCircleAll(GroundCheck.position, GroundedRadius, WhatIsGround);
        for (int i = 0; i < colliders.Length; i++)
        {
            if (colliders[i].gameObject != gameObject)
            {
                Grounded = true;
                playerAnimator.SetBool("Jumping", false);
            }
        }
    }


    public void Move(float move, bool crouch, bool jump)
    {
       
        if (!crouch)
        {

            if (Physics2D.OverlapCircle(CeilingCheck.position, CeilingRadius, WhatIsGround))
            {
                crouch = true;
            }
        }

       
        if (Grounded || AirControl)
        {

           
            if (crouch)
            {
               
                move *= CrouchSpeed;

               
                if (CrouchDisableCollider != null)
                    CrouchDisableCollider.enabled = false;
            }
            else
            {
               
                if (CrouchDisableCollider != null)
                    CrouchDisableCollider.enabled = true;
            }

          
            Vector3 targetVelocity = new Vector2(move * 10f, rigidbody.velocity.y);
           
            rigidbody.velocity = Vector3.SmoothDamp(rigidbody.velocity, targetVelocity, ref velocity, MovementSmoothing);

           
            if (move > 0 && !FacingRight)
            {
               
                Flip();
            }
          
            else if (move < 0 && FacingRight)
            {
               
                Flip();
            }
        }
       
        if (Grounded && jump)
        {
          
            Grounded = false;
            playerAnimator.SetBool("Jumping", true);
            rigidbody.AddForce(new Vector2(0f, JumpForce));
        }
    }


    private void Flip()
    {
       
        FacingRight = !FacingRight;

       
        Vector3 theScale = transform.localScale;
        theScale.x *= -1;
        transform.localScale = theScale;
    }
}
