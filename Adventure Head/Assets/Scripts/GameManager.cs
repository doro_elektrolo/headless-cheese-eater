﻿using UnityEngine;
using UnityEngine.SceneManagement;

public class GameManager : MonoBehaviour

{
    static public bool IsGameOver = false;

    public delegate void PlayerCollisionEnemy();
    public event PlayerCollisionEnemy BubbleHit;

    public delegate void PlayerCollisionToken(GameObject token);
    public event PlayerCollisionToken CubeHit;
    public event PlayerCollisionToken MeatHit;
    public event PlayerCollisionToken GlueHit;

    public delegate void PlayerCollisionWin();
    public event PlayerCollisionWin BodyHit;

    public delegate void GameEvent();
    public event GameEvent GameStart;
    public event GameEvent GameOver;

    public static GameManager Instance;

    [SerializeField]
    public static GameData SessionData;

    private void Awake()
    {
        if (Instance == null)
        {
            Instance = this;
        }
        else
        {
            Destroy(gameObject);
        }
        CreateGameData();

        DontDestroyOnLoad(gameObject);

        Instance.GameStart += ResetLives;
        Instance.BubbleHit += RemoveLives;
        Instance.BodyHit += LevelComplete;
        Instance.CubeHit += AddEnergy;
    }
    private void OnDestroy()
    {
        Instance.GameStart -= ResetGameData;
        Instance.BubbleHit -= RemoveLives;
        Instance.BodyHit -= LevelComplete;
        Instance.MeatHit -= AddEnergy;

    }
    private void CreateGameData()
    {
        if (SessionData == null)
        {
            SessionData = new GameData();
        }
    }
    public void StartLevel(int index)
    {
        GameManager.Instance.OnGameStart;
        GoToLevel(index);
        GameManager.SessionData.CurrentLevelIndex = index;
    }
    public void LevelComplete()
    {
        if (SessionData.CurrentLevelIndex < GameData.Levels.Count - 1)
        {
            GoToLevel(++SessionData.CurrentLevelIndex);
        }
        else
        {
            OnGameOver();
        }
    }
    public void GoToLevel(int levelIndex)
    {
        SceneManager.LoadScene(GameData.Levels[levelIndex]);
    }
    public void AddEnergy()
    {
        SessionData.Energy++;
    }
    public void TakeLives()
    {
        SessionData.Energy--;
    }
    public void RemoveLives()
    {
        SessionData.Energy--;

        if (SessionData.Energy <= 0)
        {
            GameOver();
        }
    }
    public void ResetLives()
    {
        SessionData.Energy = GameData.StartEnergy;
    }

    public void OnEnemyHit()
    {
        Instance.BubbleHit?.Invoke();
    }
    public void OnCubeHit(GameObject token)
    {
        Instance.CubeHit?.Invoke();
    }
    public void OnMeatHit(GameObject token)
    {
        Instance.MeatHit?.Invoke();
    }
    public void OnGlueHit(GameObject token)
    {
        Instance.GlueHit?.Invoke();
    }
    public void OnWinHit()
    {
        Instance.BodyHit?.Invoke();
    }
    public void OnGameStart()
    {
        Instance.GameStart?.Invoke();
    }



}