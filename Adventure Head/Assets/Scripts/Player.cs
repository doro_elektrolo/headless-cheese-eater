﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;


public class Player : MonoBehaviour
{


    public CharacterController controller;
    public Animator playerAnimator;

    public float runSpeed = 40f;

    float horizontalMove = 0f;
    bool jump = false;
    bool crouch = false;



    private void Start()
    {
        playerAnimator = GetComponent<Animator>();
    }
    void Update()
    {

        horizontalMove = Input.GetAxisRaw("Horizontal") * runSpeed;
        playerAnimator.SetFloat("Speed", Mathf.Abs(horizontalMove));

        if (Input.GetButtonDown("Jump"))
        {
            jump = true;
            playerAnimator.SetBool("Jumping", true);
        }

        if (Input.GetButtonDown("Crouch"))
        {
            crouch = true;
        }
        else if (Input.GetButtonUp("Crouch"))
        {
            crouch = false;
        }

    }

    void FixedUpdate()
    {

        controller.Move(horizontalMove * Time.fixedDeltaTime, crouch, jump);
        jump = false;

    }



    private void OnTriggerEnter2D(Collider2D other)
    {
        PlayerCollisions.PlayerCollided(other);
    }

    private void PlayerRespawn()
    {
        LevelManager.ThePlayer.transform.position = currentRespawnVector;
    }

    private void NewRespawnPosition(GameObject respawnPoint)
    {
        Transform respawnPointPosition = respawnPoint.GetComponent<Transform>();
        currentRespawnVector = new Vector3(respawnPointPosition.position.x, respawnPointPosition.position.y + 1f, respawnPointPosition.position.z);
    }

    private void RemovePlayer()
    {
        {
            Destroy(this);
        }

        Debug.Log(other);
        if (other.gameObject.tag == "Dead")
        {
            transform.position = RespawnPosition.position;

            GameManager.Instance.Energy--;
            thisEnergy.EnergyLost();

            if (GameManager.Instance.Energy == 0)
            {

                GameManager.IsGameOver = true;
                SceneManager.LoadScene("MainMenu");
            }

        }

        else if (other.gameObject.tag == "Checkpoint")
        {
            Transform CheckpointTransform = other.GetComponent<Transform>();
            RespawnPosition.position = CheckpointTransform.position;
            Destroy(other.gameObject);
        }

        else if (other.gameObject.tag == "WinPoint")
        {
            SceneManager.LoadScene("Level2");
            Destroy(other.gameObject);
        }

    }
}
