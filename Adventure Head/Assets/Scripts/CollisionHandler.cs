﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollisionHandler : MonoBehaviour
{


    private GameObject collectedItem;

    private void Start()
    {
        GameManager.Instance.CubeHit += DestroyOther;
        GameManager.Instance.MeatHit += DestroyOther;
        GameManager.Instance.GlueHit += DestroyOther;
        GameManager.Instance.BodyHit += RemovePlayer;
    }

    private void OnDestroy()
    {
        GameManager.Instance.CubeHit -= DestroyOther;
        GameManager.Instance.MeatHit -= DestroyOther;
        GameManager.Instance.GlueHit -= DestroyOther;
        GameManager.Instance.BodyHit -= RemovePlayer;
    }

    public void PlayerCollided(Collider2D other)
    {
        collectedItem = other.gameObject;

        switch (other.tag)
        {
            default:
                break;

            case "dead":
                EnemyCollision();
                break;

            case "cube":
                CubeCollision(collectedItem);
                break;

            case "meat":
                MeatCollision(collectedItem);
                break;

            case "glue":
                GlueCollision(collectedItem);
                break;

            case "body":
                WinCollision();
                break;

        }
    }



    private void EnemyCollision()
    {
        GameManager.Instance.OnBubbleHit();
    }

    private void CubeCollision(GameObject token)
    {

        GameManager.Instance.OnCubeHit(token);
    }

    private void MeatCollision(GameObject token)
    {
        GameManager.Instance.OnMeatHit(token);
    }
    private void GlueCollision(GameObject token)
    {
        GameManager.Instance.OnGlueHit(token);
    }

    private void WinCollision()
    {
        GameManager.Instance.OnWinHit();
    }

    private void DestroyOther(GameObject token)
    {
        if (collectedItem != null)
            Destroy(token);
    }

    private void RemovePlayer()
    {
        Destroy(this);
    }
}

