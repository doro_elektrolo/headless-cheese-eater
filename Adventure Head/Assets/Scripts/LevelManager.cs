﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class LevelManager : MonoBehaviour
{
    public Transform PlayerPlatformPosition;
    public Player PlayerPrefab;

    public static Player ThePlayer;

    public GameObject TopHUD;


    void Start()
    {
        Vector3 playerStartPosition = PlayerPlatformPosition.position;
        if (ThePlayer == null)
        {
            ThePlayer = Instantiate(PlayerPrefab, playerStartPosition, Quaternion.identity);
        }

        ThePlayer.currentRespawnVector = PlayerPlatformPosition.position;

        PlayerPlatformPosition.gameObject.SetActive(false);

        Instantiate(TopHUD);

    }
}