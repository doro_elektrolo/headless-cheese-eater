﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameData : MonoBehaviour 
{
    public int Energy;
    public const int StartEnergy = 7;

    public static List<string> Levels = new List<string>() { "MainMenu", "Level1", "Level2" };

    public int CurrentLevelIndex;
}
